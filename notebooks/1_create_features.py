import numpy as np
import pandas as pd
import random
from datetime import datetime, timedelta
import time 
from scipy.stats import truncnorm

tx_df = pd.read_csv("../data/transactions.csv")

end_date_str = '05/10/2022'
end_date = pd.to_datetime(end_date_str)

# 7D aggregated dataset
start_date = end_date - timedelta(days=6)

tx_df_7d = tx_df.loc[(tx_df.timestamp >start_date) & (tx_df.timestamp <=end_date)] # filter tx_df to 7 days
tx_df_7d = tx_df_7d.groupby("id").agg(mean_amount_7d=("tx_amount", "mean"))

# 30D aggregated dataset
start_date = end_date - timedelta(days=29)

tx_df_30d = tx_df.loc[(tx_df.timestamp >start_date) & (tx_df.timestamp <=end_date)] # filter tx_df to 30 days
tx_df_30d = tx_df_30d.groupby("id").agg(mean_amount_30d=("tx_amount", "mean"))

# Merge both dataframes using the index as a join key
agg_df = pd.merge(tx_df_7d, tx_df_30d, left_index=True, right_index=True, how='outer')

# Define timestamp column for FEAST
agg_df['feature_timestamp'] = end_date
agg_df['created'] = datetime.now()

# Write to parquet file
agg_df.fillna(0, inplace=True)
agg_df.reset_index(inplace=True)

parquet_path = '../data/aggregated_data.parquet'
csv_path = '../data/aggregated_data.csv'
print(f"Saving to parquet file {parquet_path}")

agg_df.to_csv(csv_path, index=False)
agg_df.to_parquet(parquet_path, index=False)