from datetime import timedelta

from feast import Entity, FeatureService, FeatureView, Field, FileSource, ValueType
from feast.types import Float32, Int64, String

customer_agg_tx = FileSource(
    path="/home/ssierra/sws_projects/teaching__se4ai/feast/creditscore_feast/data/aggregated_data.parquet",
    timestamp_field="feature_timestamp",
    created_timestamp_column="created",
)

customer_demographics = FileSource(
    path="/home/ssierra/sws_projects/teaching__se4ai/feast/creditscore_feast/data/customers.parquet",
    timestamp_field="timestamp",
    created_timestamp_column="created_at",
)

# Define an entity for the customers. An entity can be seen as a primary key used to
# fetch features.
customer = Entity(name="customer", join_keys=["id"], value_type=ValueType.INT64,)

customer_agg_tx_view = FeatureView(
    name="customer_agg_stats",
    entities=["customer"],
    ttl=timedelta(weeks=3),
    schema=[
        Field(name="mean_amount_7d", dtype=Float32),
        Field(name="mean_amount_30d", dtype=Float32),
    ],
    online=True,
    source=customer_agg_tx,
    tags={},
)

# Let's define an additional feature view for the customer demographics.
customer_demographics_view = FeatureView(
    name="customer_demographic_features",
    entities=["customer"],
    ttl=timedelta(weeks=52), # data is not updated as often as the aggregated data
    schema=[
        Field(name="repayed", dtype=String),
        Field(name="age", dtype=Int64),
        Field(name="gender", dtype=String),
        Field(name="have_overdraft", dtype=Int64),
        Field(name="used_overdraft", dtype=Int64),
        Field(name="member_since", dtype=Int64)
    ],
    online=True,
    source=customer_demographics,
    tags={},
)

customer_stats = FeatureService(
    name="customer_activity", features=[customer_agg_tx_view]
)
