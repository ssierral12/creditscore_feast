# creditscore_feast

Notebooks and scripts for the FEAST exercise SE4AI

# Steps for the exercise

1. Clone the repository
2. Create an environment and install: feast, scikit-learn, pandas, scipy, matplotlib
3. Inspect `notebooks/1_create_features.ipynb`
    * Generate the proposed features and save them as parquet
    * Alternatively you can use the provided script
4. Inspect `notebooks/2_create_feature_store.ipynb`
    * Following the commands in the notebook, initialize a feast repository called `creditscore_feast`
    * Navigate to the `creditscore_feast` folder
    * Delete example.py
    * Copy `feast_py/credit_features.py` to the `creditscore_feast` folder
5. Move/copy the parquet files to the `creditscore_feast/data` folder
6. Inspect `credit_features.py`
    * The `credit_features.py` script is the main script for the FEAST exercise
    * The script is used to create the features and store them in the feast repository
    * Modify the `path` parameter in both lines 7 and 13. It should point to the recently copied parquet files (`aggregated_data.parquet` and `customers.parquet`)
7. In a bash shell (or inside the `2_create_feature_store.ipynb` notebook), run the following command:
    * `feast apply`
8. Continue with the training part in the `2_create_feature_store.ipynb` notebook